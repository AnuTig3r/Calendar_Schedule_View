<?php
/**
 * Calendar Schedule View functions
 *
 * @package Calendar Schedule View plugin
 */

/**
 * Get Day Course Periods
 *
 * @param string $date    ISO date of calendar day.
 * @param int    $minutes Minutes.
 *
 * @return array Course Periods.
 */
function CalendarScheduleViewGetDayCoursePeriods( $date, $minutes )
{
	static $course_periods = array();

	if ( isset( $course_periods[ $date ] ) )
	{
		return $course_periods[ $date ];
	}

	$course_periods[ $date ] = array();

	if ( empty( $minutes ) )
	{
		return $course_periods[ $date ];
	}

	$qtr_id = GetCurrentMP( 'QTR', $date, false );

	if ( ! $qtr_id )
	{
		// Date not in a school quarter.
		return $course_periods[ $date ];
	}

	$where_sql = " AND (sp.BLOCK IS NULL
		AND position(substring('UMTWHFS' FROM cast(extract(DOW FROM acc.SCHOOL_DATE) AS INT)+1 FOR 1) IN cpsp.DAYS)>0
		OR sp.BLOCK IS NOT NULL
		AND acc.BLOCK IS NOT NULL
		AND sp.BLOCK=acc.BLOCK)";

	if ( SchoolInfo( 'NUMBER_DAYS_ROTATION' ) !== null )
	{
		$where_sql = " AND (sp.BLOCK IS NULL AND position(substring('MTWHFSU' FROM cast(
			(SELECT CASE COUNT(school_date)% " . SchoolInfo( 'NUMBER_DAYS_ROTATION' ) . " WHEN 0 THEN " . SchoolInfo( 'NUMBER_DAYS_ROTATION' ) . " ELSE COUNT(school_date)% " . SchoolInfo( 'NUMBER_DAYS_ROTATION' ) . " END AS day_number
			FROM attendance_calendar
			WHERE school_date>=(SELECT start_date FROM school_marking_periods WHERE start_date<=acc.SCHOOL_DATE AND end_date>=acc.SCHOOL_DATE AND mp='QTR' AND SCHOOL_ID=acc.SCHOOL_ID)
			AND school_date<=acc.SCHOOL_DATE
			AND SCHOOL_ID=acc.SCHOOL_ID)
		AS INT) FOR 1) IN cpsp.DAYS)>0 OR sp.BLOCK IS NOT NULL AND acc.BLOCK IS NOT NULL AND sp.BLOCK=acc.BLOCK)";
	}

	if ( User( 'PROFILE' ) === 'admin'
		&& ! empty( $_REQUEST['calendar_id'] ) )
	{
		$where_sql .= " AND acc.CALENDAR_ID='" . $_REQUEST['calendar_id'] . "'";
	}

	$course_periods_RET = DBGet( "SELECT cp.TITLE,cp.SHORT_NAME,cp.TEACHER_ID,cp.ROOM,
		c.TITLE AS COURSE_TITLE,cs.TITLE AS SUBJECT_TITLE,sp.TITLE AS PERIOD_TITLE,c.COURSE_ID,cp.COURSE_PERIOD_ID
	FROM ATTENDANCE_CALENDAR acc,COURSE_PERIODS cp,SCHOOL_PERIODS sp,COURSE_PERIOD_SCHOOL_PERIODS cpsp,
	COURSES c,COURSE_SUBJECTS cs
	WHERE cp.COURSE_PERIOD_ID=cpsp.COURSE_PERIOD_ID
	AND cp.COURSE_ID=c.COURSE_ID
	AND c.SUBJECT_ID=cs.SUBJECT_ID
	AND acc.SYEAR='" . UserSyear() . "'
	AND cp.SCHOOL_ID='" . UserSchool() . "'
	AND cp.SCHOOL_ID=acc.SCHOOL_ID
	AND cp.SYEAR=acc.SYEAR
	AND acc.SCHOOL_DATE='" . $date . "'
	AND cp.CALENDAR_ID=acc.CALENDAR_ID
	AND cp.MARKING_PERIOD_ID IN (SELECT MARKING_PERIOD_ID FROM SCHOOL_MARKING_PERIODS WHERE (MP='FY' OR MP='SEM' OR MP='QTR')
	AND SCHOOL_ID=acc.SCHOOL_ID
	AND acc.SCHOOL_DATE BETWEEN START_DATE AND END_DATE)
	AND sp.PERIOD_ID=cpsp.PERIOD_ID" .
	$where_sql .
	" ORDER BY sp.SORT_ORDER", array(), array( 'COURSE_PERIOD_ID' ) );

	$course_periods[ $date ] = (array) $course_periods_RET;

	return $course_periods[ $date ];
}


/**
 * Course Period HTML
 *
 * @param array $course_periods Course Period (multiple if various periods for same course).
 *
 * @return string HTML.
 */
function CalendarScheduleViewCoursePeriodHTML( $course_periods )
{
	static $course_colors = array(),
		$color_i = 0;

	$cp_html = '';

	$course_period_i = 0;

	// @link http://clrs.cc/
	$colors = array( '#001f3f', '#FFDC00', '#01FF70', '#F012BE', '#0074D9', '#FF4136' );

	foreach ( $course_periods as $course_period )
	{
		if ( ! $course_period_i++ )
		{
			$course_id = $course_period['COURSE_ID'];

			if ( ! isset( $course_colors[ $course_id ] ) )
			{
				$course_colors[ $course_id ] = $colors[ $color_i++ % count( $colors ) ];
			}

			if ( User( 'PROFILE' ) === 'admin'
				|| User( 'PROFILE' ) === 'teacher' )
			{
				$label = $course_period['SHORT_NAME'] . ' - ';
			}
			else
			{
				$label = $course_period['SUBJECT_TITLE'] . ' - ';
			}

			// Popup.
			$title = $course_period['COURSE_TITLE'];

			$message = '';

			if ( User( 'PROFILE' ) !== 'teacher' )
			{
				$message = GetTeacher( $course_period['TEACHER_ID'] );
			}

			if ( $course_period['ROOM'] )
			{
				if ( $message )
				{
					$message .= '<br />';
				}

				$message .= _( 'Room' ) . ': ' . $course_period['ROOM'];
			}
		}
		else
		{
			$label .= ', ';
		}

		$label .= $course_period['PERIOD_TITLE'];
	}

	$cp_html .= '<div style="border-left-color:' . $course_colors[ $course_id ] . '"><span>';

	$cp_html .= MakeTipMessage( $message, $title, $label ) . '</span></div>';

	return $cp_html;
}
